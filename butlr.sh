#!/usr/bin/env bash

# creator: Dillon King
# Dependcies: fzf jq
# Date: 05-May-2024

# where butlr things are stored
butlrdir="/home/$USER/butlr/"

# default fzf butlr prompt
function butlrfzf {
    fzf -i --prompt "Butlr: " --reverse --border "rounded"
}

function main {
    clear
    actions=("Quote" "Habit Tracker" "Weather recomendation" "Todo" "Exit")
    action=$(printf "%s\n" "${actions[@]}" | butlrfzf)
    case $action in
        "Quote")
            quotes
            ;;
        "Habit Tracker")
            habits
            ;;
        "Weather recomendation")
            weather
            ;;
        "Todo")
            todo
            ;;
        "Exit")
            exit 0
            ;;
    esac
}

function quotes {
  quotes=("I can take it. The tougher it gets, the cooler I get." "You are not being buried you are being planted" "When you are winning you are not as good as you think you are, when you are losing you are not as bad as you think you are?" "Unix is simple but it takes a genius to understand its simplicity" "build bridges not walls" )
   len=$(echo ${#quotes[@]})
   rand=$((RANDOM % len))
   echo -e "\033[35m${quotes[rand]}\033[0m"
   read -n 1 -s -r -p "Press any key to continue"
   main
}

function habits {
    clear
    habits=$(cat $butlrdir/info/habits)
    if [[ -z $habits ]]; then
        echo -e "\033[35mYou have no habits would you like to create one?(y/n)\033[0m" && read -n 1 -s -r new
        case $new in
            "y")
                read -p "Please type name of habit: " newhabit
                echo "$newhabit 1 days" >> $butlrdir/info/habits
                habits
                ;;
            "n")
                main
                ;;
        esac
    else
        keybinds=("a - add new habit" "m - go to main menu" "s - add a day to a habit" "r - reset habit to one day" "d - delete a habit")
        echo -e "\033[4mHabits\033[0m"
        printf "%s\n" "${habits[@]}" | lolcat
        echo ""
        echo -e "\033[4mKeybinds\033[0m"
        printf "%s\n" "${keybinds[@]}" | lolcat
        read -n 1 -s -r bind
        case $bind in
            "m")
                main
                ;;
            "a")
                read -p "Please type name of habit: " newhabit
                echo "$newhabit 1 days" >> $butlrdir/info/habits
                habits
                ;;
            "s")
                habits=$(cat $butlrdir/info/habits)
                addday=$(printf "%s\n" "${habits[@]}" | fzf -i --prompt "habit to add a day to: " --reverse --border "rounded")
                addday=$(echo $addday | awk '{print $1}')
                current=$(grep $addday $butlrdir/info/habits | awk '{print $2}')
                newvalue=$((current+1))
                sed -i "s/$addday $current/$addday $newvalue/g" $butlrdir/info/habits
                habits
                ;;
            "r")
                habits=$(cat $butlrdir/info/habits)
                addday=$(printf "%s\n" "${habits[@]}" | fzf -i --prompt "habit to add a day to: " --reverse --border "rounded")
                addday=$(echo $addday | awk '{print $1}')
                current=$(grep $addday $butlrdir/info/habits | awk '{print $2}')
                sed -i "s/$addday $current/$addday 1/g" $butlrdir/info/habits
                habits
                ;;
            "d")
               dtodo=$(printf "%s\n" "${habits[@]}" | fzf -i --prompt "habit entrie to delete: " --reverse --border "rounded")
               dtodo=$(grep "$dtodo" $butlrdir/info/habits)
               sed -i "/$dtodo/d" $butlrdir/info/habits
               habits
        esac
    fi
}

function weather {
    wcondition=$(wget -qO- "http://wttr.in/?format=j1" | jq -r ."current_condition[0]."weatherDesc[0]."value""")
    [[ $wcondition == "Light rain" ]] && echo -e "\033[35mThere is $wcondition you should bring a coat\033[0m"
    [[ $wcondition == "Light drizzle" ]] && echo -e "\033[35mThere is $wcondition you should bring a coat\033[0m"
    [[ $wcondition == "Sunny" ]] && echo -e "\033[35mIt is $wcondition you should wear cool clothes\033[0m"
    [[ $wcondition == "Clear" ]] && echo -e "\033[35mIt is $wcondition have a nice day\033[0m"
    sleep 1
    read -n 1 -s -r -p "press any key to continue"
    main
}

function todo {
    clear
    todos=$(cat $butlrdir/info/todo)
    if [[ -z $todos ]]; then
        echo -e "\033[35mYou have no todo items would you like to create one?(y/n)\033[0m" && read -n 1 -s -r new
        case $new in
            "y")
                read -p "Please enter a todo item" newtodo
                echo "$newtodo" >> $butlrdir/info/todo
                todo
                ;;
            "n")
                main
                ;;
        esac
    else
        keybinds=("a - add new todo item" "m - go to main menu" "d - delete a todo item")
        echo -e "\033[4mTodos\033[0m"
        printf "%s\n" "${todos[@]}"
        echo ""
        echo -e "\033[4mKeybinds\033[0m"
        printf "%s\n" "${keybinds[@]}"
        read -n 1 -s -r bind
        case $bind in
            "m")
                main
                ;;
            "a")
                read -p "Please type name of todo: " newtodo
                echo "$newtodo" >> $butlrdir/info/todo
                todo
                ;;
            "d")
               dtodo=$(printf "%s\n" "${todos[@]}" | fzf -i --prompt "todo entrie to delete: " --reverse --border "rounded")
               sed -i "/$dtodo/d" $butlrdir/info/todo
               todo
                ;;
        esac
    fi
}

# run the program
main
